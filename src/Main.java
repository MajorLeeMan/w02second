/*
this project must use a List, a Queue, a Set, and a Tree, and showcase each
your program should show you know how to use generics
implement the comparator interface to show how to sort one of your collections???

PLAN:
5 ask the user what kind of list they would like displayed (list, queue, set, tree)
6 ask again if user enters invalid number
7 display the requested list
8 repeat step 5
if no, exit the program
 */

import java.util.*;
//import java.util.Scanner;
//import java.util.InputMismatchException;

public class Main {

    public static void main(String[] args) {
        //starting variables
        Scanner keyboard = new Scanner(System.in);
        String userInput;
        int userNum;
        String [] arrayItems = {"item1", "item2", "item3", "item4", "item5"};
        int [] arrayImportance = {5, 4, 3, 2, 1};

        //we are going to ask the user to input 5 items and their values
        for (int i=0; i<arrayItems.length; i++) {
            //1 ask the user to add an item to the array
            System.out.println("Please add 5 items to the array.");
            System.out.println("Item "+(i+1)+": ");
            //2 prompt for text entry and importance entry
            userInput = keyboard.next();
            //3 add the item into the list
            arrayItems[i] = userInput;
            //ask the user to input the items value
            System.out.println("How important is this item, 1 being least valuable and 10 being most valuable.");
            try {
                userNum = keyboard.nextInt();
                //attempt to place the value in the array
                arrayImportance[i] = userNum;
            } catch (InputMismatchException var11) {
                //if entry was not an integer, return the user to the beginning of the for statement
                System.out.println("Not an interger. 1-10 please.");
                keyboard.nextLine();
                --i;

            }
            //4 repeat step 1 until for statement is complete
        }
        System.out.println("Now that we have your 5 items, how would you like to view them?");
        System.out.println("Press 1=list, 2=alphabetical queue, 3=sets, 4=tree");
        userNum = keyboard.nextInt();
        switch (userNum) {
            case 1: //view array as a list
                List list = new ArrayList();
                for (int i=0; i<arrayItems.length; i++) {
                    list.add(arrayItems[i]);
                    System.out.println(arrayItems[i]);
                }
                break;

            case 2: //view as a alphabetical queue
                Queue queue = new PriorityQueue();
                for (int i=0; i<arrayItems.length; i++) {
                    queue.add(arrayItems[i]);
                    }
                Iterator pattern = queue.iterator();
                while(pattern.hasNext()) {
                    System.out.println(queue.poll());
                    }
                break;

            case 3: //view as set, omitting duplicates
                Set set = new TreeSet();
                for (int i=0; i<arrayItems.length; i++) {
                    set.add(arrayItems[i]);
                }
                System.out.println("Duplicates omitted.");
                for (Object items : set) {
                    System.out.println((String)items);
                }
                break;

            case 4: //view as tree
                break;

            default:
                System.out.println("Not a valid option, try again.");
                break;
        }
    }
}
